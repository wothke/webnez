/*
* Converts SNG('SCC MUSIXX' song format) to KSS (adapted from https://nezplug.sourceforge.net/ )

  Known limitation: songs currently loop endlessly. In order to add song end detection, a similar approach
  as the one used for MBM could be added - but it doesn't seem to be worth the effort.
*/
class SngConverter {
	constructor() {
		this._kssHead = new Uint8Array([
			0x4b, 0x53, 0x53, 0x58, 0x00, 0xc0, 0x52, 0x00, 0x00, 0xc0, 0x9f, 0xfd, 0x00, 0x02, 0x10, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x3e, 0xc9, 0x32, 0x9f, 0xfd, 0xaf, 0xd3, 0xfe, 0x2a, 0x03, 0x80, 0xed, 0x5b, 0x01, 0x80, 0x37,
			0xed, 0x52, 0x11, 0x88, 0x00, 0x19, 0x7d, 0xe6, 0x80, 0x6f, 0xe5, 0xe5, 0xc1, 0x21, 0x00, 0x80,
			0x11, 0xf9, 0xcf, 0xed, 0xb0, 0xd1, 0xe5, 0x21, 0x01, 0x40, 0x37, 0xed, 0x52, 0xe5, 0xc1, 0xe1,
			0x11, 0x00, 0x00, 0xed, 0xb0, 0x3e, 0x01, 0xd3, 0xfe, 0x21, 0x00, 0x80, 0x01, 0x00, 0x40, 0xed,
			0xb0, 0x3e, 0xc9, 0x32, 0x33, 0xd5, 0x32, 0x76, 0xd5, 0x97, 0x32, 0xb8, 0xd0, 0xd3, 0xfe, 0xc3,
			0x03, 0xd0
		]);

		this._musixxReplay  = new Uint8Array([	//  MUSIXX REPLAY ROUTINE v1.2  By M.Spoor
			0xfe, 0x00, 0xd0, 0x1a, 0xd6, 0x00, 0xd0, 0xc3, 0x16, 0xd5, 0xc3, 0x6d, 0xd0, 0x00, 0x00, 0x1f,
			0x11, 0x1b, 0xd6, 0x0e, 0x1a, 0xcd, 0x7d, 0xf3, 0xaf, 0x32, 0x8b, 0xd5, 0x21, 0x02, 0xd6, 0x11,
			0x03, 0xd6, 0x01, 0x1a, 0x00, 0x36, 0x00, 0xed, 0xb0, 0x11, 0xf6, 0xd5, 0x0e, 0x0f, 0xcd, 0x7d,
			0xf3, 0xb7, 0xc0, 0x21, 0x00, 0x01, 0x22, 0x04, 0xd6, 0x06, 0x80, 0x11, 0x00, 0x00, 0xc5, 0xd5,
			0x11, 0xf6, 0xd5, 0x21, 0x01, 0x00, 0x0e, 0x27, 0xcd, 0x7d, 0xf3, 0xd1, 0xc1, 0xb7, 0x20, 0x1c,
			0xc5, 0x21, 0x1b, 0xd6, 0x01, 0x00, 0x01, 0xe5, 0xd5, 0xc5, 0xcd, 0x33, 0xd5, 0xc1, 0xd1, 0xe1,
			0xed, 0xb0, 0xe5, 0xd5, 0xcd, 0x76, 0xd5, 0xd1, 0xe1, 0xc1, 0x10, 0xd2, 0x11, 0xf6, 0xd5, 0x0e,
			0x10, 0xc3, 0x7d, 0xf3, 0x21, 0x9f, 0xfd, 0x11, 0x81, 0xd1, 0x01, 0x05, 0x00, 0xed, 0xb0, 0xdb,
			0xa8, 0xf5, 0xe6, 0xcf, 0xf5, 0xc6, 0x10, 0xcd, 0x97, 0xd0, 0xf1, 0xf5, 0xc6, 0x20, 0xcd, 0x97,
			0xd0, 0xf1, 0xf5, 0xc6, 0x30, 0xcd, 0x97, 0xd0, 0xf1, 0xf1, 0xd3, 0xa8, 0xaf, 0xc9, 0xd3, 0xa8,
			0x4f, 0x3a, 0x00, 0x90, 0x57, 0xaf, 0x32, 0x00, 0x90, 0x3a, 0x00, 0x90, 0xb7, 0x28, 0x0c, 0x47,
			0xee, 0xff, 0x32, 0x00, 0x90, 0x3a, 0x00, 0x90, 0xb8, 0x20, 0x05, 0x7a, 0x32, 0x00, 0x90, 0xc9,
			0x79, 0xe6, 0x30, 0x32, 0x8b, 0xd5, 0xf1, 0xf1, 0xf1, 0xd3, 0xa8, 0x3e, 0x08, 0x32, 0x84, 0xd5,
			0xaf, 0x32, 0x86, 0xd5, 0xcd, 0x33, 0xd5, 0x21, 0x00, 0x98, 0x06, 0x8a, 0x36, 0x00, 0x23, 0x10,
			0xfb, 0xaf, 0x32, 0xe0, 0x98, 0x3c, 0x32, 0x85, 0xd5, 0x32, 0x83, 0xd5, 0x3a, 0x01, 0x02, 0x3d,
			0x32, 0x07, 0xd0, 0xcd, 0xee, 0xd4, 0xcd, 0x76, 0xd5, 0x21, 0x8c, 0xd5, 0x11, 0x8d, 0xd5, 0x01,
			0x69, 0x00, 0x36, 0x00, 0xed, 0xb0, 0xf3, 0x3e, 0xc3, 0x21, 0x0f, 0xd1, 0x32, 0x9f, 0xfd, 0x22,
			0xa0, 0xfd, 0xfb, 0x3e, 0x01, 0xc9, 0x3a, 0x83, 0xd5, 0x3d, 0x20, 0x06, 0xcd, 0x0f, 0xd2, 0x3a,
			0x84, 0xd5, 0x32, 0x83, 0xd5, 0x21, 0xd2, 0xd5, 0x06, 0x00, 0xc5, 0x7e, 0x23, 0xe5, 0xb7, 0x28,
			0x0f, 0xfe, 0x01, 0xcc, 0x86, 0xd1, 0xfe, 0x02, 0xcc, 0x86, 0xd1, 0xfe, 0x05, 0xcc, 0xd8, 0xd1,
			0xe1, 0xc1, 0x04, 0x23, 0x78, 0xfe, 0x05, 0x20, 0xe1, 0x21, 0x85, 0xd5, 0x35, 0x20, 0x30, 0x36,
			0x03, 0x21, 0xe1, 0xd5, 0x11, 0x8c, 0xd5, 0x06, 0x05, 0x34, 0xcb, 0x46, 0x23, 0x7e, 0x23, 0xeb,
			0xf5, 0xb7, 0x20, 0x04, 0xf1, 0x23, 0x18, 0x13, 0xf1, 0x28, 0x08, 0x86, 0x77, 0x23, 0x30, 0x01,
			0x34, 0x18, 0x08, 0x4f, 0x7e, 0x91, 0x77, 0x23, 0x30, 0x01, 0x35, 0x23, 0xeb, 0x10, 0xda, 0xcd,
			0x33, 0xd5, 0xcd, 0xc2, 0xd4, 0xcd, 0x76, 0xd5, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc5, 0xd5, 0xe5,
			0x78, 0x87, 0x5f, 0x16, 0x00, 0x21, 0x8c, 0xd5, 0x19, 0xe5, 0x80, 0x5f, 0x16, 0x00, 0x21, 0xaa,
			0xd5, 0x19, 0x7e, 0x08, 0x23, 0x46, 0x23, 0x7e, 0xb7, 0x28, 0x02, 0x35, 0x04, 0xe1, 0x08, 0xb7,
			0x28, 0x0e, 0xe5, 0xcd, 0xca, 0xd1, 0xe1, 0x11, 0x14, 0x00, 0x19, 0xcd, 0xca, 0xd1, 0x18, 0x0c,
			0xe5, 0xcd, 0xd1, 0xd1, 0xe1, 0x11, 0x14, 0x00, 0x19, 0xcd, 0xd1, 0xd1, 0xe1, 0xd1, 0xc1, 0xaf,
			0xc9, 0x7e, 0x90, 0x77, 0xd0, 0x23, 0x35, 0xc9, 0x7e, 0x80, 0x77, 0xd0, 0x23, 0x34, 0xc9, 0x58,
			0x16, 0x00, 0x21, 0xc3, 0xd5, 0x19, 0xe5, 0x78, 0x87, 0x5f, 0x16, 0x00, 0x21, 0xc8, 0xd5, 0x19,
			0xd1, 0x46, 0x23, 0x7e, 0xb7, 0x28, 0x02, 0x35, 0x04, 0xeb, 0xcb, 0x78, 0xcb, 0xb8, 0x7e, 0x20,
			0x0c, 0x80, 0xe6, 0x0f, 0xbe, 0x28, 0x04, 0x30, 0x02, 0x3e, 0x0f, 0x77, 0xc9, 0x90, 0x30, 0x01,
			0xaf, 0xe6, 0x0f, 0x77, 0xaf, 0xc9, 0xcd, 0x33, 0xd5, 0x2a, 0x87, 0xd5, 0xe5, 0x11, 0x8c, 0xd5,
			0xdd, 0x21, 0xb9, 0xd5, 0x06, 0x05, 0xc5, 0x46, 0x23, 0x4e, 0x23, 0x79, 0xb0, 0x20, 0x12, 0x13,
			0x13, 0x18, 0x4e, 0x12, 0xf5, 0xe5, 0xd5, 0xeb, 0x11, 0x14, 0x00, 0x19, 0x77, 0xd1, 0xe1, 0xf1,
			0xc9, 0xdd, 0x7e, 0x00, 0xb7, 0x78, 0x28, 0x05, 0xdd, 0x96, 0x01, 0x18, 0x03, 0xdd, 0x86, 0x01,
			0xcd, 0x2c, 0xd2, 0x13, 0x30, 0x0a, 0xdd, 0x7e, 0x00, 0xb7, 0x28, 0x03, 0x0d, 0x18, 0x01, 0x0c,
			0x79, 0xcd, 0x2c, 0xd2, 0x13, 0xc1, 0xc5, 0xe5, 0xd5, 0x3e, 0x05, 0x90, 0x4f, 0xcb, 0x21, 0x06,
			0x00, 0x21, 0xe1, 0xd5, 0x09, 0xeb, 0x21, 0xeb, 0xd5, 0x09, 0x01, 0x02, 0x00, 0xed, 0xb0, 0xd1,
			0xe1, 0x23, 0x23, 0x23, 0xdd, 0x23, 0xdd, 0x23, 0xc1, 0x10, 0x9b, 0xe1, 0x23, 0x23, 0xe5, 0x23,
			0x06, 0x05, 0x11, 0xc3, 0xd5, 0xc5, 0x7e, 0xe6, 0xf0, 0x1f, 0x1f, 0x1f, 0x1f, 0x12, 0x13, 0x01,
			0x05, 0x00, 0x09, 0xc1, 0x78, 0xfe, 0x02, 0x20, 0x01, 0x2b, 0x10, 0xe9, 0xe1, 0xe5, 0x06, 0x04,
			0x11, 0x00, 0x98, 0xc5, 0x7e, 0xb7, 0x28, 0x0f, 0xe5, 0xd5, 0x21, 0xdc, 0xd5, 0x58, 0x16, 0x00,
			0x1d, 0x19, 0xbe, 0xd1, 0x20, 0x09, 0xe1, 0x01, 0x20, 0x00, 0xeb, 0x09, 0xeb, 0x18, 0x1f, 0x77,
			0xd5, 0x21, 0x00, 0x00, 0x11, 0x28, 0x00, 0x47, 0x19, 0x10, 0xfd, 0xed, 0x52, 0xd1, 0x01, 0x20,
			0x00, 0xaf, 0x32, 0x8f, 0x98, 0xed, 0xb0, 0x3a, 0x08, 0xd0, 0x32, 0x8f, 0x98, 0xe1, 0x01, 0x05,
			0x00, 0x09, 0xc1, 0x10, 0xbe, 0xe1, 0x23, 0x11, 0xd2, 0xd5, 0x06, 0x05, 0xc5, 0x7e, 0x23, 0xe6,
			0x0f, 0xfe, 0x0f, 0xcc, 0x65, 0xd3, 0xfe, 0x0e, 0xcc, 0x74, 0xd3, 0xfe, 0x0c, 0xcc, 0x80, 0xd3,
			0xfe, 0x0b, 0xcc, 0xa8, 0xd3, 0xfe, 0x0a, 0xcc, 0x94, 0xd3, 0xfe, 0x09, 0xcc, 0x7a, 0xd3, 0xfe,
			0x07, 0xcc, 0x04, 0xd4, 0xfe, 0x06, 0xcc, 0xfc, 0xd3, 0xfe, 0x05, 0xcc, 0x61, 0xd4, 0xfe, 0x04,
			0xcc, 0xbc, 0xd3, 0xfe, 0x03, 0xcc, 0xc1, 0xd3, 0xfe, 0x02, 0xcc, 0x96, 0xd4, 0xfe, 0x01, 0xcc,
			0x91, 0xd4, 0x12, 0x13, 0x7e, 0x12, 0x13, 0x01, 0x04, 0x00, 0x09, 0xc1, 0x78, 0xfe, 0x02, 0x20,
			0x01, 0x2b, 0x10, 0xa8, 0x2b, 0x2b, 0x2b, 0x22, 0x87, 0xd5, 0x3a, 0x86, 0xd5, 0x3c, 0xfe, 0x40,
			0x20, 0x04, 0xcd, 0xee, 0xd4, 0xaf, 0x32, 0x86, 0xd5, 0xc3, 0x76, 0xd5, 0x7e, 0xb7, 0x28, 0x04,
			0xfe, 0x01, 0x20, 0x02, 0x3e, 0x02, 0x32, 0x84, 0xd5, 0xaf, 0xc9, 0x7e, 0x32, 0x06, 0xd0, 0xaf,
			0xc9, 0x7e, 0x32, 0xe0, 0x98, 0xaf, 0xc9, 0x7e, 0xb7, 0x20, 0x08, 0xdb, 0xaa, 0xf6, 0x40, 0xd3,
			0xaa, 0xaf, 0xc9, 0xdb, 0xaa, 0xe6, 0xbf, 0xd3, 0xaa, 0xaf, 0xc9, 0x3a, 0x86, 0xd5, 0xb7, 0xc8,
			0xe1, 0xcd, 0xee, 0xd4, 0x3a, 0x07, 0xd0, 0xaf, 0x32, 0x86, 0xd5, 0xf1, 0xc3, 0x12, 0xd2, 0x3a,
			0x86, 0xd5, 0xb7, 0xc8, 0xf1, 0x3a, 0x80, 0x07, 0x3d, 0x96, 0x7e, 0x30, 0x01, 0xaf, 0xcd, 0xfa,
			0xd4, 0x18, 0xe4, 0xd5, 0x16, 0x00, 0x18, 0x03, 0xd5, 0x16, 0x01, 0xc5, 0xe5, 0x7e, 0x08, 0xd5,
			0x3e, 0x05, 0x90, 0xcb, 0x07, 0x4f, 0x06, 0x00, 0x21, 0xe1, 0xd5, 0x09, 0xf1, 0xc5, 0x77, 0x23,
			0x08, 0x77, 0x2b, 0x54, 0x5d, 0x01, 0x0a, 0x00, 0x09, 0xeb, 0x01, 0x02, 0x00, 0xed, 0xb0, 0xc1,
			0x21, 0x8c, 0xd5, 0x09, 0xeb, 0x21, 0xa0, 0xd5, 0x09, 0x01, 0x02, 0x00, 0xed, 0xb0, 0xe1, 0xc1,
			0xd1, 0xaf, 0xc9, 0xc5, 0xcd, 0x4a, 0xd4, 0x0e, 0x01, 0x18, 0x06, 0xc5, 0xcd, 0x4a, 0xd4, 0x0e,
			0x00, 0x7e, 0x08, 0xd5, 0xe5, 0x3e, 0x05, 0x90, 0x17, 0x5f, 0x16, 0x00, 0x21, 0xb9, 0xd5, 0x19,
			0x79, 0x77, 0x23, 0x08, 0x77, 0xf5, 0x08, 0xf5, 0x21, 0xa0, 0xd5, 0xcd, 0x36, 0xd4, 0xf1, 0x08,
			0xf1, 0x08, 0x21, 0x8c, 0xd5, 0xcd, 0x36, 0xd4, 0xe1, 0xd1, 0xc1, 0xaf, 0xc9, 0x19, 0xb7, 0x28,
			0x09, 0x08, 0x4f, 0x7e, 0x91, 0x77, 0x23, 0xd0, 0x35, 0xc9, 0x08, 0x86, 0x77, 0x23, 0xd0, 0x34,
			0xc9, 0xc5, 0xd5, 0xe5, 0x3e, 0x05, 0x90, 0x17, 0x5f, 0x16, 0x00, 0x21, 0xb9, 0xd5, 0x19, 0x7e,
			0x3c, 0xe6, 0x01, 0x08, 0x23, 0x7e, 0x18, 0xbd, 0xc5, 0xd5, 0xe5, 0x7e, 0x08, 0x3e, 0x05, 0x90,
			0x87, 0x21, 0xc8, 0xd5, 0x06, 0x00, 0x4f, 0x09, 0x3a, 0x84, 0xd5, 0x4f, 0x08, 0xf5, 0xe6, 0xf0,
			0xb7, 0x28, 0x02, 0x3e, 0x80, 0x47, 0xf1, 0xe6, 0x0f, 0x04, 0x91, 0x30, 0xfc, 0x81, 0x05, 0x70,
			0x23, 0x77, 0xe1, 0xd1, 0xc1, 0x3e, 0x05, 0xc9, 0xc5, 0x0e, 0x01, 0x18, 0x03, 0xc5, 0x0e, 0x00,
			0xf5, 0xd5, 0xe5, 0x7e, 0x08, 0x3e, 0x05, 0x90, 0x47, 0x87, 0x80, 0x5f, 0x16, 0x00, 0x21, 0xaa,
			0xd5, 0x19, 0x71, 0x23, 0x06, 0x00, 0x3a, 0x84, 0xd5, 0x4f, 0x08, 0x04, 0x91, 0x30, 0xfc, 0x81,
			0x05, 0x70, 0x23, 0x77, 0xe1, 0xd1, 0xf1, 0xc1, 0xc9, 0x21, 0xc3, 0xd5, 0x11, 0x8a, 0x98, 0x01,
			0x05, 0x00, 0xed, 0xb0, 0x3a, 0x08, 0xd0, 0x32, 0x8f, 0x98, 0xdd, 0x21, 0x80, 0x98, 0x21, 0x96,
			0xd5, 0x11, 0x8c, 0xd5, 0x06, 0x0a, 0x1a, 0xbe, 0x28, 0x04, 0xdd, 0x77, 0x00, 0x77, 0x23, 0x13,
			0xdd, 0x23, 0x10, 0xf2, 0xc9, 0x3a, 0x80, 0x07, 0x47, 0x3a, 0x07, 0xd0, 0x3c, 0xb8, 0x20, 0x01,
			0xaf, 0x32, 0x07, 0xd0, 0x4f, 0x06, 0x00, 0x21, 0x81, 0x07, 0x09, 0x7e, 0x47, 0x04, 0x21, 0xe5,
			0x07, 0x11, 0x00, 0x06, 0x19, 0x10, 0xfd, 0xed, 0x52, 0x22, 0x87, 0xd5, 0xc9, 0xf3, 0x21, 0x81,
			0xd1, 0x11, 0x9f, 0xfd, 0x01, 0x05, 0x00, 0xed, 0xb0, 0xfb, 0xcd, 0x33, 0xd5, 0x21, 0x80, 0x98,
			0x06, 0x70, 0x36, 0x00, 0x23, 0x10, 0xfb, 0xc3, 0x76, 0xd5, 0xdb, 0xa8, 0x32, 0x89, 0xd5, 0x3a,
			0xff, 0xff, 0xee, 0xff, 0x32, 0x8a, 0xd5, 0x3a, 0x42, 0xf3, 0x26, 0x40, 0xcd, 0x24, 0x00, 0xf3,
			0x3a, 0x41, 0xf3, 0xe6, 0x03, 0x5f, 0xdb, 0xa8, 0xe6, 0xfc, 0x83, 0xe6, 0xcf, 0x47, 0x3a, 0x8b,
			0xd5, 0x80, 0xd3, 0xa8, 0x3a, 0x41, 0xf3, 0xe6, 0x0c, 0x1f, 0x1f, 0x5f, 0x3a, 0xff, 0xff, 0xee,
			0xff, 0xe6, 0xfc, 0x83, 0x32, 0xff, 0xff, 0x3e, 0x3f, 0x32, 0x00, 0x90, 0xc9, 0x3a, 0x89, 0xd5,
			0xd3, 0xa8, 0x3a, 0x8a, 0xd5, 0x32, 0xff, 0xff, 0xfb, 0xc9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20,
			0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x53, 0x4e, 0x47, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20
		]);
	}

	updateSongInfo(result) {
		result.track= 0;
		result.copyright= "";
		result.tracks= 1;
	}

	convert(filename, data) {	// data is an Uint8Array
		// the SNG seems to contain no song or author info

		let converted = new Uint8Array(this._kssHead.length + this._musixxReplay.length +  + data.length);
		converted.set(this._kssHead,		0);
		converted.set(this._musixxReplay,	this._kssHead.length);
		converted.set(data,					this._kssHead.length + this._musixxReplay.length);


		if (filename.includes("50Hz"))
			converted[0xf] |= 0x40;

		filename = filename.substring(0, filename.length-3) + "kss";

		return [0, filename, converted];
	}
}
